# Write your code here
import random
from collections import defaultdict


class RockPaperScissorsGame:
    options = []
    rating = defaultdict(int)

    def start(self):
        user_name = input("Enter your name:")
        with open("rating.txt") as input_file:
            for line in input_file.readlines():
                try:
                    name, score = line.split()
                    self.rating[name] = int(score)
                except ValueError:
                    continue
        print(f"Hello, {user_name}")
        options = input()
        if not options:
            self.options = ["scissors", "rock", "paper"]
        else:
            self.options = options.split(",")

        center = len(self.options)//2

        print("Okay, let's start")
        while True:
            user_option = input()
            full_options = ["!exit", "!rating"] + self.options
            if user_option not in full_options:
                print("Invalid input")
                continue
            if user_option == "!exit":
                print("Bye!")
                break
            if user_option == "!rating":
                score = self.rating[user_name]
                print(f"Your rating: {score}")
                continue
            computer_choice = random.choice(self.options)
            if computer_choice == user_option:
                # It's a draw
                self.rating[user_name] += 50
                print(f"There is a draw ({computer_choice})")
            else:
                user_index = self.options.index(user_option)
                centered_options = self.options[user_index + 1:] + self.options[:user_index]
                lose = centered_options[:center]
                win = centered_options[center:]
                if computer_choice in win:
                    self.rating[user_name] += 100
                    print(f"Well done. The computer chose {computer_choice} and failed")
                elif computer_choice in lose:
                    print(f"Sorry, but the computer chose {computer_choice}")


RockPaperScissorsGame().start()
